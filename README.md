Nix flake template for Haskell projects adapted from https://github.com/srid/haskell-flake/

# Using
* Create a directory for your project
* In the project directory, run `nix flake init -t gitlab:redshift78/haskell-flake#example`, or #brick
* Rename example.cabal to a name suited to your project.
* Change instances of example in the .cabal file to your project name.
* Change instances of example in the flake.nix file to your project name.

# Building a container
* Uncomment either the single layer container, or the layered container section in flake.nix
* Build with `nix build .#container`
* Import into podman with `gunzip -c result | podman load`
* Import into docker with `docker load < result`
