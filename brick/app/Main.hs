module Main where

import AppUI

import Brick.Main
import Control.Monad (void)

main :: IO ()
main = void $ defaultMain defaultApp defaultUIState
