Template for Haskell Brick projects

# Using
* Rename example.cabal to a name suited to your project.
* Change instances of example in the .cabal file to your project name.
* Change instances of example in the flake.nix file to your project name.

# Building a container
* Uncomment either the single layer container, or the layered container section in flake.nix
* Build with `nix build .#container`
* Import into podman with `gunzip -c result | podman load`
* Import into docker with `docker load < result`
