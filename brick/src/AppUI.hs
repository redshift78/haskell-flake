{-# LANGUAGE TemplateHaskell #-}

module AppUI
  ( UIState (..)
  , defaultUIState
  , defaultApp
  ) where

import Brick.Main
import Brick.Types
import Brick.Util (on)
import Brick.Widgets.Core
import Lens.Micro.TH

import qualified Graphics.Vty as V
import qualified Brick.AttrMap as A

data UIEvent  = PlaceHolderEvent deriving (Eq, Show)
type UINames  = ()

newtype UIState = UIState
  { _placeHolderState :: Int
  } deriving (Eq, Show)
makeLenses ''UIState

defaultUIState :: UIState
defaultUIState = UIState
  { _placeHolderState = 0
  }

defaultApp :: App UIState UIEvent UINames
defaultApp = App
  { appDraw         = drawUI
  , appChooseCursor = \_ _ -> Nothing
  , appHandleEvent  = eventHandler
  , appStartEvent   = return ()
  , appAttrMap      = attrMap
  }

drawUI :: UIState -> [ Widget UINames ]
drawUI state = [ str "Hello Brick!" ]

eventHandler :: BrickEvent UINames UIEvent -> EventM UINames UIState ()
eventHandler (VtyEvent e)
  | e == V.EvKey V.KEsc         []  = halt
  | e == V.EvKey (V.KChar 'q')  []  = halt
  | otherwise                       = return ()

eventHandler _  = return ()

attrMap :: UIState -> A.AttrMap
attrMap state = A.attrMap V.defAttr
  [ (A.attrName "text",    V.white `on` V.black)
  ]
